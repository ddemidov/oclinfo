#include <iostream>
#include "oclinfo.h"

int main() {
    int n = num_devices();

    std::cout << "Devices: " << n << std::endl;
    for(int i = 0; i < n; ++i) {
        char device[256];

        device_name(i, device, 256);

        std::cout << i << ": " << device << std::endl
            << "   memory: " << device_memory(i) << std::endl
            << "   cores:  " << device_cores(i)  << std::endl
            << "   score:  " << device_score(i)  << std::endl;
    }

    std::cout << std::endl;
    std::cout << "CPU cores: " << cpu_cores() << std::endl;
    std::cout << "CPU score: " << cpu_score() << std::endl;
}
